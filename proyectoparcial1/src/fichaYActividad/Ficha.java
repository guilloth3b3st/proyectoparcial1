/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fichaYActividad;
import java.util.ArrayList;
import retoGym.Categoria;
import usuarios.ClienteGym;
import java.util.HashMap;

/**
 *
 * @author Castillo
 */
public class Ficha {
    //String es Actividad, ArrayList<Boolean> es para denotar si se realizo o no en las diferentes categorias
    HashMap<String,ArrayList<Boolean>> actRealizadas;
    ClienteGym cliente;
    String idReto;
    boolean inicial;

    public Ficha(HashMap<String, ArrayList<Boolean>> actRealizadas, ClienteGym cliente, String idReto, boolean inicial) {
        this.actRealizadas = actRealizadas;
        this.cliente = cliente;
        this.idReto = idReto;
        this.inicial = inicial;
    }

    public HashMap<String, ArrayList<Boolean>> getActRealizadas() {
        return actRealizadas;
    }

    public void setActRealizadas(HashMap<String, ArrayList<Boolean>> actRealizadas) {
        this.actRealizadas = actRealizadas;
    }

    public ClienteGym getCliente() {
        return cliente;
    }

    public void setCliente(ClienteGym cliente) {
        this.cliente = cliente;
    }

    public String getIdReto() {
        return idReto;
    }

    public void setIdReto(String idReto) {
        this.idReto = idReto;
    }

    public boolean isInicial() {
        return inicial;
    }

    public void setInicial(boolean inicial) {
        this.inicial = inicial;
    }
    
    
}
