/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fichaYActividad;
import java.util.ArrayList;
import retoGym.Categoria;
/**
 *
 * @author Castillo
 */
public class Actividad {
    ArrayList<String> actRealizadas;
    
    public Actividad (ArrayList<String> actRealizadas){
        this.actRealizadas = new ArrayList<>();
        for (String act: actRealizadas){
            actRealizadas.add(act);
        }
    }

    public ArrayList<String> getActRealizadas() {
        return actRealizadas;
    }

    public void setActRealizadas(ArrayList<String> actRealizadas) {
        this.actRealizadas = actRealizadas;
    }
    
    
}