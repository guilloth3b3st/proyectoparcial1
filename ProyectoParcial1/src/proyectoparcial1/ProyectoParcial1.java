
package proyectoparcial1;
import fichaYActividad.Actividad;
import fichaYActividad.Ficha;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import retoGym.Categoria;
import retoGym.RetoGym;
import usuarios.ClienteGym;
import usuarios.PersonalGym;
import usuarios.TipoCargo;
import usuarios.Usuario;
import utilidades.Utilidades;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Grupo
 */

public class ProyectoParcial1 { 
    
    static ArrayList<Usuario> personal;
    static ArrayList<Usuario> clientes;
    static ArrayList<RetoGym> retos;
    static ArrayList<Ficha> fichas;
    
    public static void inicializarSistema(){
        personal = Utilidades.leerArchivo("empleados.csv");
        clientes = Utilidades.leerArchivo("clientes-1.csv");
        retos = new ArrayList<RetoGym>();
        fichas = new ArrayList<>();
        
    }
    
    public static void main(String[] args) {
        
        //Cargar todos los archivos en ArrayList
        inicializarSistema();
        
        Scanner input = new Scanner(System.in);
        boolean logeado = true;

        while(logeado){
            
            //Login
            System.out.println("Bienvenido al Gimnasio!");
            System.out.print("Ingrese su nombre de Usuario: ");
            String user = input.next();
            System.out.print("Ingrese su contraseña: ");
            String pass = input.next();
            Usuario usuarioLogeado = null;
            
            for (Usuario u : personal) {
                if (u.getUsuario().equals(user) && u.getContrasena().equals(pass)) {
                    usuarioLogeado = u;
                }
            }

            for(Usuario u: clientes){
                if(u.getUsuario().equals(user) && u.getContrasena().equals(pass)){
                    usuarioLogeado = u;
                }
            }
            
            if(usuarioLogeado!=null){
                switch (usuarioLogeado.getNivel()) {
                    case ADMINISTRADOR:
                        mostrarAdminView();
                        break;
                    case PERSONAL:
                        mostrarPersonalView();
                        break;
                    case CLIENTE:
                        mostrarClientesView();
                        break;
                    default:
                        System.out.println("Usuario y contraseña validos, pero no se reconoce nivel!");
                        break;
                }
            }else{
                System.out.println("Usuario no encontrado o clave incorrecta.");
            }
            System.out.println("");
        }
        
        
        System.out.println("Saliendo del sistema de Gimnasio.");
    }
        
    
    public static void mostrarAdminView(){
       System.out.println("Admin");
        boolean estado = true;
        Scanner input = new Scanner(System.in);
        while(estado){
            System.out.println("1. Crear Reto");
            System.out.println("2. Inscribir usuario en reto");
            System.out.println("3. Consultar inscritos en reto");
            System.out.println("4. Consultar fichas y actividades de usuario");
            System.out.println("5. Registrar ganador de reto");
            System.out.println("6. Salir");
            System.out.print("Opcion: ");
            String nom = input.next();
            switch(nom){
                case "1":
                    int cupoMax = 15;
                    boolean band = true;
                    SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYYY");
                    Calendar fechaII = Calendar.getInstance();
                    Calendar fechaFI = Calendar.getInstance();
                    Calendar fechaI = Calendar.getInstance();
                    Calendar fechaF = Calendar.getInstance();
                    while(band){
                        try{
                            System.out.print("Fecha de Inicio de Inscripciones(DD/MM/YYYY): ");
                            fechaII.setTime(sdf.parse(input.next()));
                            System.out.print("Fecha de Fin de Inscripciones(DD/MM/YYYY: ");
                            fechaFI.setTime(sdf.parse(input.next()));
                            System.out.print("Fecha de Inicio de Reto(DD/MM/YYYY: ");
                            fechaI.setTime(sdf.parse(input.next()));
                            System.out.print("Fecha de Fin de Reto(DD/MM/YYYY): ");
                            fechaF.setTime(sdf.parse(input.next()));
                        }catch(ParseException e){
                            e.printStackTrace();
                        }
                        
                        System.out.println("Ingrese ID del terapista fisico: ");
                        String terapista = input.next();
                        System.out.println("Ingrese ID del nutricionista: ");
                        String nutricionista = input.next();
                        System.out.println("Ingrese ID del entrenador: ");
                        String entrenador = input.next();
                        int temp = 0;
                        for(Usuario u: personal){
                            if(u instanceof PersonalGym){
                                if(((PersonalGym) u).getId().equals(terapista) && ((PersonalGym) u).getCargo().equals(TipoCargo.TERAPISTA)){
                                    //enviar correo
                                    temp++;
                                }else if(((PersonalGym) u).getId().equals(nutricionista) && ((PersonalGym) u).getCargo().equals(TipoCargo.NUTRICIONISTA)){
                                    temp++;
                                    //enviar correo
                                }else if(((PersonalGym) u).getId().equals(entrenador) && ((PersonalGym) u).getCargo().equals(TipoCargo.ENTRENADOR)){
                                    temp++;
                                    //enviar correo
                                }
                            }
                        }
                        if(temp == 3){
                            String idReto;
                            if(retos.isEmpty()){
                                idReto = "1";
                            }else{
                                idReto = String.valueOf(Integer.parseInt(retos.get(retos.size()-1).getIdReto())+1);
                            }
                            RetoGym reto;
                            reto = new RetoGym(fechaII, fechaFI, fechaI, fechaF, terapista, nutricionista, entrenador, cupoMax, new HashMap<>(),idReto, new ArrayList<>());
                            retos.add(reto);
                            band = false;
                        }
                        
                        if(band){
                            System.out.println("ID no encontrado. ¿Los ID's empiezan sin ceros?");
                        }
                    }
                    break;
                case "2":
                    System.out.print("Ingrese el ID cliente: ");
                    String idCliente = input.next();
                    ClienteGym cliente = null;
                    for(Usuario u: clientes){
                        if(u instanceof ClienteGym){
                            if(u.getUsuario().equals(idCliente)){
                                cliente = (ClienteGym) u;
                            }
                        }
                    }
                    Categoria categoria = null;
                    if(cliente != null){
                        boolean ban = true;
                        while(ban){
                            System.out.println("Escoga la categoria: ");
                            System.out.println("1. "+Categoria.FATBURN);
                            System.out.println("2. "+Categoria.HYPERGAIN);
                            System.out.println("3. "+Categoria.MUSCLEGAIN);
                            System.out.println("4. "+Categoria.SUGARFREE);
                            int cat = input.nextInt();
                            switch(cat){
                                case 1:
                                    categoria = Categoria.FATBURN;
                                    ban = false;
                                    break;
                                case 2:
                                    categoria = Categoria.HYPERGAIN;
                                    ban = false;
                                    break;
                                case 3:
                                    categoria = Categoria.MUSCLEGAIN;
                                    ban = false;
                                    break;
                                case 4:
                                    categoria = Categoria.SUGARFREE;
                                    ban = false;
                                    break;
                                default:
                                    System.out.println("Escoga un opcion valida");
                                    break;
                            }
                        }
                    }else{
                        System.out.println("Cliente no encontrado");
                    }
                    
                    if(retos.isEmpty()){
                        System.out.println("Cree un reto primero");
                    }
                    
                    //Verificacion previa a inscripcion
                    for(RetoGym r: retos){
                        if(!(r.getClientes().containsKey(cliente) || cliente.getFechaDeExpMenp().compareTo(r.getFechaCReto())<0)){
                            int catTemp = 0;
                            for(Categoria c: r.getClientes().values()){
                                if(categoria.equals(c)){
                                    catTemp++;
                                }
                            }
                            if(catTemp<15){
                                //Registrar
                                r.getClientes().put(cliente, categoria);
                                byte[] array = new byte[8]; // length is bounded by 7
                                new Random().nextBytes(array);
                                String generatedString = new String(array, Charset.forName("UTF-8"));
                                cliente.setContrasena(generatedString);
                                //enviar correo
                            }else{
                                System.out.println("Categoria Llena");
                            }
                        }
                    }
                    break;
                case "3":
                    System.out.print("Ingrese Id de reto: ");
                    String id = input.next();
                    RetoGym re = null;
                    for(RetoGym r: retos){
                        if(r.getIdReto().equals(id)){
                            re = r;
                        }
                    }
                    if(re!=null){
                        System.out.println("Terapista reto: "+re.getIdTerapista());
                        System.out.println("Nutricionista reto: "+re.getIdNutricionista());
                        System.out.println("Entrenador reto: "+re.getIdEntrenador());
                        System.out.println("Fecha Inicio Inscripcion: "+re.getFechaInInscrip().DAY_OF_MONTH+"/"+re.getFechaInInscrip().MONTH+"/"+re.getFechaInInscrip().YEAR);
                        System.out.println("Fecha Fin Inscripcion: "+re.getFechaCInscrip().DAY_OF_MONTH+"/"+re.getFechaCInscrip().MONTH+"/"+re.getFechaCInscrip().YEAR);
                        System.out.println("Fecha Inicio Reto: "+re.getFechaIReto().DAY_OF_MONTH+"/"+re.getFechaIReto().MONTH+"/"+re.getFechaIReto().YEAR);
                        System.out.println("Fecha Fin Reto: "+re.getFechaCReto().DAY_OF_MONTH+"/"+re.getFechaCReto().MONTH+"/"+re.getFechaCReto().YEAR);
                        Iterator it = re.getClientes().keySet().iterator();
                        while(it.hasNext()){
                          ClienteGym key = (ClienteGym) it.next();
                            System.out.println("Id Cliente: " + key.getUsuario());
                            System.out.println("Nombre Cliente : " + key.getNombre());
                            System.out.println("Email Cliente: " + key.getEmail());
                            System.out.println("Categoria: "+re.getClientes().get(key));
                        }
                    }else{
                        System.out.println("Reto no encontrado");
                    }
                    
                    break;
                case "4":
                    mostrarClientesView();
                    break;
                case "5":
                    System.out.print("Ingrese Id de reto: ");
                    String idR = input.next();
                    RetoGym ret = null;
                    for(RetoGym r: retos){
                        if(r.getIdReto().equals(idR)){
                            ret = r;
                        }
                    }
                    System.out.print("Ingrese el ID cliente: ");
                    String idCli = input.next();
                    ClienteGym client = null;
                    for(Usuario u: clientes){
                        if(u instanceof ClienteGym){
                            if(u.getUsuario().equals(idCli)){
                                client = (ClienteGym) u;
                            }
                        }
                    }
                    if(ret!=null && client!=null){
                        boolean seg = true;
                        while(seg){
                            System.out.println("Eliga la categoria del ganador: ");
                            System.out.println("1. "+Categoria.FATBURN+", 2. "+Categoria.HYPERGAIN+", 3. "+Categoria.MUSCLEGAIN+", 4. "+Categoria.SUGARFREE);
                            int opcion = input.nextInt();
                            switch(opcion){
                                case 1:
                                    ret.getGanador().set(0, client);
                                    seg = false;
                                    break;
                                case 2:
                                    ret.getGanador().set(1, client);
                                    seg = false;
                                    break;
                                case 3:
                                    ret.getGanador().set(2, client);
                                    seg = false;
                                    break;
                                case 4:
                                    ret.getGanador().set(3, client);
                                    seg = false;
                                    break;
                                default:
                                    System.out.println("Escoga una opcion valida");
                            }
                        }
                    }else{
                        System.out.println("Reto o cliente no encontrado.");
                    }
                    break;
                case "6":
                    estado = false;
                    break;
                default:
                    System.out.println("Ingrese una opcion correcta");
                    break;
            }
        }
    }
    
    public static void mostrarPersonalView(){
        boolean present = false;
        System.out.println("Personal");
        boolean estado = true;
        Scanner input = new Scanner(System.in);
        while(estado){
            System.out.println("1. Ingreso de ficha Inicial");
            System.out.println("2. Registro de actividad de usuario");
            System.out.println("3. Ingreso de ficha final");
            System.out.println("4. Salir");
            System.out.print("Opcion: ");
            String nom = input.next();
            switch(nom){
                case "1":
                    present = false;
                    System.out.print("Ingrese Id de reto: ");
                    String idR = input.next();
                    RetoGym ret = null;
                    for(RetoGym r: retos){
                        if(r.getIdReto().equals(idR)){
                            ret = r;
                        }
                    }
                    System.out.println("Ingrese el id Cliente: ");
                    String idCli = input.next();
                    ClienteGym client = null;
                    for(Usuario u: clientes){
                        if(u instanceof ClienteGym){
                            if(u.getUsuario().equals(idCli)){
                                client = (ClienteGym) u;
                            }
                        }
                    }
                    if(ret!=null && client!=null && ret.getFechaCInscrip().compareTo(Calendar.getInstance())>=0){
                        for(Ficha f: fichas){
                            if(f.getCliente().equals(client) && f.getIdReto().equals(ret.getIdReto()) && f.isInicial()){
                                present = true;
                                System.out.println("Id Reto: "+ret.getIdReto());
                                System.out.println("Cliente: "+client.getNombre());
                                Iterator it = f.getActRealizadas().keySet().iterator();
                                while(it.hasNext()){
                                    String key = (String) it.next();
                                    switch (ret.getClientes().get(client)) {
                                        case FATBURN:
                                            System.out.println(Categoria.FATBURN +" "+f.getActRealizadas().get(key).get(0));
                                            break;
                                        case HYPERGAIN:
                                            System.out.println(Categoria.HYPERGAIN+" "+f.getActRealizadas().get(key).get(1));
                                            break;
                                        case MUSCLEGAIN:
                                            System.out.println(Categoria.MUSCLEGAIN+" "+f.getActRealizadas().get(key).get(2));
                                            break;
                                        case SUGARFREE:
                                            System.out.println(Categoria.SUGARFREE+" "+f.getActRealizadas().get(key).get(3));
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        if(!present){
                            HashMap<String,ArrayList<Boolean>> temp = new HashMap<>();
                            Ficha ficha = new Ficha(temp, client, idR,true);
                            fichas.add(ficha);
                            System.out.println("Ficha por defecto creada.");
                        }
                    }else{
                        System.out.println("Reto o cliente no encontrado.");
                    }
                    break;
                case "2":
                    present = false;
                    System.out.print("Ingrese Id de reto: ");
                    String id = input.next();
                    RetoGym re = null;
                    for(RetoGym r: retos){
                        if(r.getIdReto().equals(id)){
                            re = r;
                        }
                    }
                    System.out.println("Ingrese el id Cliente: ");
                    String idCl = input.next();
                    ClienteGym clien = null;
                    for(Usuario u: clientes){
                        if(u instanceof ClienteGym){
                            if(u.getUsuario().equals(idCl)){
                                clien = (ClienteGym) u;
                            }
                        }
                    }
                    System.out.println("Ingrese el nombre de Actividad: ");
                    String act = input.next();
                    if(re!=null && clien!=null && re.getFechaCInscrip().compareTo(Calendar.getInstance())>=0){
                        for(Ficha f: fichas){
                            if(f.getCliente().equals(clien) && f.getIdReto().equals(re.getIdReto())){
                                present = true;
                                if(!f.getActRealizadas().containsKey(act)){
                                    ArrayList<Boolean> cate = new ArrayList<>();
                                    cate.add(false);
                                    cate.add(false);
                                    cate.add(false);
                                    cate.add(false);
                                    f.getActRealizadas().put(act, cate);
                                }else{
                                    System.out.println("Actividad repetida");
                                }
                            }
                        }
                        if(!present){
                            System.out.println("No se encontro ninguna ficha relacionada con un cliente");
                        }
                    }else{
                        System.out.println("Reto o cliente no encontrado");
                    }
                    break;
                case "3":
                    System.out.print("Ingrese Id de reto: ");
                    String idReto = input.next();
                    RetoGym reto = null;
                    for(RetoGym r: retos){
                        if(r.getIdReto().equals(idReto)){
                            reto = r;
                        }
                    }
                    System.out.println("Ingrese el id Cliente: ");
                    String idCliente = input.next();
                    ClienteGym cliente = null;
                    for(Usuario u: clientes){
                        if(u instanceof ClienteGym){
                            if(u.getUsuario().equals(idCliente)){
                                cliente = (ClienteGym) u;
                            }
                        }
                    }
                    if(reto!=null && cliente!=null && reto.getFechaCReto().compareTo(Calendar.getInstance())>=0){
                        for(Ficha f: fichas){
                            if(f.getCliente().equals(cliente) && f.getIdReto().equals(reto.getIdReto()) && !f.isInicial()){
                                System.out.println("Id Reto: "+reto.getIdReto());
                                System.out.println("Cliente: "+cliente.getNombre());
                                Iterator it = f.getActRealizadas().keySet().iterator();
                                present = true;
                                while(it.hasNext()){
                                    String key = (String) it.next();
                                    switch (reto.getClientes().get(cliente)) {
                                        case FATBURN:
                                            System.out.println(Categoria.FATBURN +" "+f.getActRealizadas().get(key).get(0));
                                            break;
                                        case HYPERGAIN:
                                            System.out.println(Categoria.HYPERGAIN+" "+f.getActRealizadas().get(key).get(1));
                                            break;
                                        case MUSCLEGAIN:
                                            System.out.println(Categoria.MUSCLEGAIN+" "+f.getActRealizadas().get(key).get(2));
                                            break;
                                        case SUGARFREE:
                                            System.out.println(Categoria.SUGARFREE+" "+f.getActRealizadas().get(key).get(3));
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        if(!present){
                            HashMap<String,ArrayList<Boolean>> temp = new HashMap<>();
                            Ficha ficha = new Ficha(temp, cliente, idReto,false);
                            fichas.add(ficha);
                            System.out.println("Ficha por defecto creada.");
                        }
                    }else{
                        System.out.println("Reto o cliente no encontrado.");
                    }
                    break;
                case "4":
                    estado = false;
                    break;
                default:
                    System.out.println("Ingrese una opcion correcta");
                    break;
            }
        }
    }
    
    public static void mostrarClientesView(){
        System.out.println("Cliente");
        boolean estado = true;
        Scanner input = new Scanner(System.in);
        while(estado){
            System.out.println("1. Consulta de ficha Inicial");
            System.out.println("2. Consulta de actividades realizadas en el reto");
            System.out.println("3. Consulta de ficha final");
            System.out.println("4. Salir");
            System.out.print("Opcion: ");
            String nom = input.next();
            boolean band = false;
            switch(nom){
                case "1":
                    band = false;
                    System.out.println("Ingrese el id Cliente: ");
                    String idCliente = input.next();
                    ClienteGym cliente = null;
                    for(Usuario u: clientes){
                        if(u instanceof ClienteGym){
                            if(u.getUsuario().equals(idCliente)){
                                cliente = (ClienteGym) u;
                            }
                        }
                    }
                    if(cliente!=null){
                        for(Ficha f: fichas){
                            if(f.getCliente().equals(cliente) && f.isInicial()){
                                band = true;
                                System.out.println("Id del Reto: "+f.getIdReto());
                                Iterator it = f.getActRealizadas().keySet().iterator();
                                while(it.hasNext()){
                                  String key = (String) it.next();
                                  System.out.println("Actividad: " + key + ", Realizada: " + f.getActRealizadas().get(key));
                                }
                            }
                        }
                        if(!band){
                            System.out.println("No se encontro ninguna ficha relacionada con un cliente.");
                        }
                    }else{
                        System.out.println("Cliente no encontrado.");
                    }
                    break;
                case "2":
                    band = false;
                    System.out.println("Ingrese el id Cliente: ");
                    String idC = input.next();
                    ClienteGym cli = null;
                    for(Usuario u: clientes){
                        if(u instanceof ClienteGym){
                            if(u.getUsuario().equals(idC)){
                                cli = (ClienteGym) u;
                            }
                        }
                    }
                    if(cli!=null){
                        for(Ficha f: fichas){
                            if(f.getCliente().equals(cli)){
                                band = true;
                                System.out.println("Id del Reto: "+f.getIdReto());
                                Iterator it = f.getActRealizadas().keySet().iterator();
                                while(it.hasNext()){
                                  String key = (String) it.next();
                                  System.out.println("Actividad: " + key+", Ficha Inicial: "+f.isInicial());
                                }
                            }
                        }
                        if(!band){
                            System.out.println("No se encontro ninguna ficha relacionada con un cliente");
                        }
                    }else{
                        System.out.println("Cliente no encontrado");
                    }
                    break;
                case "3":
                    band = false;
                    System.out.println("Ingrese el id Cliente: ");
                    String idClient = input.next();
                    ClienteGym client = null;
                    for(Usuario u: clientes){
                        if(u instanceof ClienteGym){
                            if(u.getUsuario().equals(idClient)){
                                client = (ClienteGym) u;
                            }
                        }
                    }
                    if(client!=null){
                        for(Ficha f: fichas){
                            if(f.getCliente().equals(client) && !f.isInicial()){
                                band = true;
                                System.out.println("Id del Reto: "+f.getIdReto());
                                Iterator it = f.getActRealizadas().keySet().iterator();
                                while(it.hasNext()){
                                  String key = (String) it.next();
                                  System.out.println("Actividad: " + key + ", Realizada: " + f.getActRealizadas().get(key));
                                }
                            }
                        }
                        if(!band){
                            System.out.println("No se encontro ninguna ficha relacionada con un cliente.");
                        }
                    }else{
                        System.out.println("Cliente no encontrado.");
                    }
                    break;
                case "4":
                    estado = false;
                    break;
                default:
                    System.out.println("Ingrese una opcion correcta");
                    break;
            }
        }
    }
 
}
