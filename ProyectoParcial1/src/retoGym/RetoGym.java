
package retoGym;
import usuarios.ClienteGym;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Castillo
 */
public class RetoGym {
    private Calendar fechaInInscrip;
    private Calendar fechaCInscrip;
    private Calendar fechaIReto;
    private Calendar fechaCReto;
    private String idTerapista;
    private String idNutricionista;
    private String idEntrenador;
    private Map<ClienteGym,Categoria> clientes;
    private String idReto;
    private List<ClienteGym> ganador;
    
    public RetoGym(Calendar fechaInInscrip, Calendar fechaCInscrip, Calendar fechaIReto, Calendar fechaCReto, String idTerapista, String idNutricionista, String idEntrenador, int cupoMaximo,Map<ClienteGym,Categoria> clientes, String idReto, List<ClienteGym> ganador) {
        this.fechaInInscrip = fechaInInscrip;
        this.fechaCInscrip = fechaCInscrip;
        this.fechaIReto = fechaIReto;
        this.fechaCReto = fechaCReto;
        this.idTerapista = idTerapista;
        this.idNutricionista = idNutricionista;
        this.idEntrenador = idEntrenador;
        this.clientes = clientes;
        this.idReto = idReto;
        this.ganador = ganador;
    }

    public Calendar getFechaInInscrip() {
        return fechaInInscrip;
    }

    public void setFechaInInscrip(Calendar fechaInInscrip) {
        this.fechaInInscrip = fechaInInscrip;
    }

    public Calendar getFechaCInscrip() {
        return fechaCInscrip;
    }

    public void setFechaCInscrip(Calendar fechaCInscrip) {
        this.fechaCInscrip = fechaCInscrip;
    }

    public Calendar getFechaIReto() {
        return fechaIReto;
    }

    public void setFechaIReto(Calendar fechaIReto) {
        this.fechaIReto = fechaIReto;
    }

    public Calendar getFechaCReto() {
        return fechaCReto;
    }

    public void setFechaCReto(Calendar fechaCReto) {
        this.fechaCReto = fechaCReto;
    }

    public String getIdTerapista() {
        return idTerapista;
    }

    public void setIdTerapista(String idTerapista) {
        this.idTerapista = idTerapista;
    }

    public String getIdNutricionista() {
        return idNutricionista;
    }

    public void setIdNutricionista(String idNutricionista) {
        this.idNutricionista = idNutricionista;
    }

    public String getIdEntrenador() {
        return idEntrenador;
    }

    public void setIdEntrenador(String idEntrenador) {
        this.idEntrenador = idEntrenador;
    }

    public String getIdReto() {
        return idReto;
    }

    public void setIdReto(String idReto) {
        this.idReto = idReto;
    }

    public List<ClienteGym> getGanador() {
        return ganador;
    }

    public void setGanador(List<ClienteGym> ganador) {
        this.ganador = ganador;
    }

    public Map<ClienteGym, Categoria> getClientes() {
        return clientes;
    }

    public void setClientes(Map<ClienteGym, Categoria> clientes) {
        this.clientes = clientes;
    }

    
    
}
