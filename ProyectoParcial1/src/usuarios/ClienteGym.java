/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package usuarios;

import java.util.Calendar;

/**
 *
 * @author nicho
 */
public class ClienteGym extends Usuario {
    private String codigo, direccion, email ;
    private Calendar fechaExpMenp, fechaIniMenp;
    private int telefono;
    //Categorias categoria
    public ClienteGym (String usuario, String codigo, String direccion, int telefono, String email, Calendar fechaExpMenp, String contrasena, String nombre, TipoNivel nivel){
        super(usuario, contrasena, nombre, nivel);
        this.codigo = codigo;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.fechaIniMenp = fechaIniMenp;
        this.fechaExpMenp = fechaExpMenp;
}
    public String getCodigo(){
        return codigo;
    }
    public void setCodigo(){
        this.codigo = codigo;
    }
    public String getDireccion(){
        return direccion;
    }
    public void setDireccion(){
        this.direccion =direccion ;
    }
    public Calendar getFechaDeExpMenp(){
            return fechaExpMenp;
    }
    public void setFechaDeExpMenp(){
        this.fechaExpMenp =fechaExpMenp;
    }
    public int getTelefono(){
        return telefono;
    }
    public void setTelefono(){
        this.telefono = telefono;
    }
    
    public String getEmail(){
        return email;
    }
    
    public void setEmail(){
        this.email = email;
    }
 
}
