/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utilidades;
import usuarios.Admin;
import usuarios.ClienteGym;
import usuarios.PersonalGym;
import usuarios.TipoCargo;
import usuarios.TipoNivel;
import usuarios.Usuario;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author nicho
 */
public class Utilidades {
    //método para lectura de archivo csv
    //devuelve un arraylist
    public static ArrayList<Usuario> leerArchivo(String nombreArchivo){
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYYY");
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        int cont =0;//para contar las lineas e ignorar la primera
        if(nombreArchivo.equals("empleados.csv") || nombreArchivo.equals("usuarios-1.csv")){
            return cargarPersonal("empleados.csv","usuarios-1.csv");
        }
        try {
            br = new BufferedReader(new FileReader("src/archivos/"+nombreArchivo));
            boolean ocasional = true;
            while ((line = br.readLine()) != null) {
                if (cont!=0 && ! (line.trim().equals("") || line.trim().equals("\n"))){
                   String[] datos = line.split(cvsSplitBy);

                    if(nombreArchivo.equals("clientes-1.csv")){
                        Calendar fecha = Calendar.getInstance();
                        try {
                            fecha.setTime(sdf.parse(datos[5]));
                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                        Usuario cliente = new ClienteGym(datos[0],datos[0],datos[2], Integer.parseInt(datos[3]),datos[4],fecha,"0",datos[1],TipoNivel.valueOf("CLIENTE"));
                        lista.add(cliente);
                    }else{
                        System.out.println("Archivo no encontrado en las carpetas!");
                        return lista;
                    }
                }
                cont++;

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

         return lista;

    }

   private static ArrayList<Usuario> cargarPersonal(String ar1, String ar2){
        ArrayList<Usuario> listaPersonal = new ArrayList<Usuario>();
        
        BufferedReader br = null;
        String line = "";
        //Se define separador ","
        String cvsSplitBy = ",";
        int cont =0;//para contar las lineas e ignorar la primera
        try {
            br = new BufferedReader(new FileReader("src/archivos/"+ar2));
            boolean ocasional = true;
            while ((line = br.readLine()) != null) {
                if (cont!=0 && ! (line.trim().equals("") || line.trim().equals("\n"))){
                    String[] datos = line.split(cvsSplitBy);
                    Usuario personal;
                    if(datos[2].toUpperCase().equals("ADMIN")){
                        personal = new Admin("ID", "DIRECCION", "11", "AA", TipoCargo.ADMINISTRADOR, datos[0], datos[1], "NOMBRE", TipoNivel.ADMINISTRADOR);
                        listaPersonal.add(personal);
                    }else if(datos[2].toUpperCase().equals("EMPLEADO")){
                        personal = new PersonalGym("ID", "DIRECCION", "11", "AA", TipoCargo.ENTRENADOR, datos[0], datos[1], "NOMBRE", TipoNivel.PERSONAL);
                        listaPersonal.add(personal);
                    }else{
                        System.out.println("Nivel no identificado!");
                    }
                }
                cont++;

            }

         } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } 
        br = null; 
        line = "";
        //Se define separador ","
        cvsSplitBy = ",";
        cont =0;//para contar las lineas e ignorar la primera
        try {
            br = new BufferedReader(new FileReader("src/archivos/"+ar1));
            boolean ocasional = true;
            while ((line = br.readLine()) != null) {
                if (cont!=0 && ! (line.trim().equals("") || line.trim().equals("\n"))){
                    String[] datos = line.split(cvsSplitBy);
                    for(int i=0;i<listaPersonal.size();i++){
                        Usuario personal = listaPersonal.get(i);
                        if(personal.getUsuario().equals(datos[6])){
                            if(personal instanceof Admin){
                                ((Admin) personal).setId(datos[0]);
                                ((Admin) personal).setNombre(datos[1]);
                                ((Admin) personal).setDireccion(datos[2]);
                                ((Admin) personal).setTelefono(datos[3]);
                                ((Admin) personal).setEmail(datos[4]);
                                ((Admin) personal).setCargo(TipoCargo.valueOf(datos[5].toUpperCase()));
                            }else if(personal instanceof PersonalGym){
                                ((PersonalGym) personal).setId(datos[0]);
                                ((PersonalGym) personal).setNombre(datos[1]);
                                ((PersonalGym) personal).setDireccion(datos[2]);
                                ((PersonalGym) personal).setTelefono(datos[3]);
                                ((PersonalGym) personal).setEmail(datos[4]);
                                ((PersonalGym) personal).setCargo(TipoCargo.valueOf(datos[5].toUpperCase()));
                            }else{
                                System.out.println("Instancia no reconocida.");
                            }
                        }
                    }
                }
                cont++;

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

         return listaPersonal; 

}
}